# Librespot

To build Librespot, [create a new release](https://git.jacknet.io/TerribleCodeClub/librespot-build/releases/new).  
  
[Drone](https://drone.jacknet.io/TerribleCodeClub/librespot-build/) will build Librespot for Linux (x64 static binary) and attach it to the release if successful.